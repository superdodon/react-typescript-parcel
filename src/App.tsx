import { configureStore, createSlice } from '@reduxjs/toolkit'

const counterSlice = createSlice({
  name: 'counter',
  initialState: 0,
  reducers: {
    increment: state => state + 1,
    decrement: state => state - 1
  }
})

const { actions, reducer } = counterSlice
const { increment, decrement } = actions

const store = configureStore({ reducer: reducer })
const valueEl = document.getElementById('value');

function render() {
  valueEl.innerHTML = store.getState().toString();
}

render();
store.subscribe(render);

document.getElementById('increment').addEventListener('click', () => {
  store.dispatch(increment())
})
document.getElementById('decrement').addEventListener('click', () => {
  store.dispatch(decrement())
})
document.getElementById("incrementIfOdd").addEventListener("click", function () {
  if (store.getState() % 2 !== 0) {
    store.dispatch(increment());
  }
});
document.getElementById("incrementAsync").addEventListener("click", function () {
  setTimeout(function () {
    store.dispatch(increment());
  }, 1000);
});