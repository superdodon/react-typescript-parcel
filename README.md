# react-typescript-parcel
Basic React, Redux, Typescript, Jest, prototype with Parcel JS.

Example counter application implemented with the Redux toolkit as a starting point.

## install
`npm install`

## develop
`npm run dev`

`npm run test`

## bundle for production
`npm run prod`